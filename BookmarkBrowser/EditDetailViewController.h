//
//  EditDetailViewController.h
//  BookmarkBrowser
//
//  Created by Matt on 15/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookmarkEntry.h"
@protocol EditDetailViewControllerDelegate <NSObject>
-(void) addBookmarkEntry:(BookmarkEntry *) bme;
-(BookmarkEntry*)getNewBME;
@end

@interface EditDetailViewController : UIViewController


@property (nonatomic, weak) IBOutlet id <EditDetailViewControllerDelegate> delegate;

@property Boolean adding;
@property int index;
@property (weak, nonatomic) IBOutlet UITextField *NameOutlet;
@property (weak, nonatomic) IBOutlet UITextField *URLOutlet;
@property (weak, nonatomic) IBOutlet UITextField *ImageURLOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *ImageOutlet;
@property (weak, nonatomic) BookmarkEntry *cBME;

@end
