//
//  MasterViewController.m
//  BookmarkBrowser
//
//  Created by Matt on 8/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import "MasterViewController.h"
#import "WebDetailViewController.h"
#import "EditDetailViewController.h"
#import "BookmarkEntry.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    NSString *fileName = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"bookmarks.db"];
    NSURL *fileURL = [NSURL fileURLWithPath:fileName];
    self.bmDocument = [[UIManagedDocument alloc] initWithFileURL:fileURL];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:fileName])
    {
        //load it
        [self.bmDocument openWithCompletionHandler:^(BOOL success) {
            if(!success)
            {
                NSLog(@"oops2");
            }
            else
            {
                [self fetchData];
                NSLog(@"Loaded existing bookmarks.db");
            }
        }];
        
    }
    else
    {
        //create it
        [self.bmDocument saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            if(!success)
            {
                NSLog(@"oops");
            }
            else
            {
                [self fetchData];
                NSLog(@"created blank bookmarks.db");
            }
        }];
    }
    
    
}

- (void)fetchData
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BookmarkEntry"];
    [request setFetchBatchSize:20];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey: @"name" ascending:YES]];
    self.controller = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.bmDocument.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    self.controller.delegate = self;
    
    NSError *error = nil;
    if([self.controller performFetch:&error])
        NSLog(@"fetched");
    else
        NSLog(@"oops");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

-(void) configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *) indexPath
{
    BookmarkEntry *bme = [self.controller objectAtIndexPath:indexPath];
    cell.textLabel.text = bme.name;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self setEditing:NO animated:YES];
    [self.navigationItem.leftBarButtonItem setTitle:@"Edit"];
    [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
}

- (void)viewWillAppear
{
    //[self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    //[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int test = [[self.controller sections] count];
    return test;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.controller  sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    BookmarkEntry *managedObject = [self.controller  objectAtIndexPath:indexPath];
    // Configure the cell with data from the managed object.
    cell.textLabel.text=managedObject.name;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.controller  sections] objectAtIndex:section];
    return [sectionInfo name];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self.controller  sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.controller  sectionForSectionIndexTitle:title atIndex:index];
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


//Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
     //[bookmarks.Entries removeObjectAtIndex:indexPath.row];
     //[Bookmarks Save];
     //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     [self.tableView reloadData];
 
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender    {
    if([[segue identifier] isEqualToString:@"Select"])
    {
        NSIndexPath *i = [self.tableView indexPathForSelectedRow];
        WebDetailViewController *dvc = [segue destinationViewController];
        BookmarkEntry *bm = [self.controller objectAtIndexPath:i];
        NSString *url = bm.url;
        //Bookmarks *bm = [Bookmarks getBookmarks];
        //NSString *url = [[bm.Entries objectAtIndex:i.row] URL];
        [[dvc URLBox] setText:url];
        
    }
    else if([[segue identifier] isEqualToString:@"Add"])
    {
        EditDetailViewController *evc = [segue destinationViewController];
        evc.adding = YES;
        [evc setDelegate:self];
        
    } else {
        EditDetailViewController *evc = [segue destinationViewController];
        evc.adding = NO;
        NSIndexPath *i = [self.tableView indexPathForCell:sender];
        evc.index = i.row;
    }
    
    //int x = 2;//DetailViewController *detailViewController = [segue destinationViewController];
    //detailViewController = segue.destinationViewController;
}
- (IBAction)EditPressed:(id)sender {
    if([self isEditing])
    {
        [self setEditing:NO animated:YES];
        [sender setTitle:@"Edit"];
        [sender setStyle:UIBarButtonItemStylePlain];
    }
    else
    {
        [self setEditing:YES animated:YES];
        [sender setTitle:@"Done"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStyleDone];
    }
}


-(void)addBookmarkEntry:(BookmarkEntry *)bme
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BookmarkEntry"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO]];
    NSError *error = nil;
    BookmarkEntry *bookmarkEntryToEdit = [[self.bmDocument.managedObjectContext executeFetchRequest:request error:&error] lastObject];
    bookmarkEntryToEdit = bme;
    
    if([self.bmDocument.managedObjectContext save:&error])
    {
        NSLog([error.userInfo description]);
    
    }
    else
    {
        NSLog(@"nope");
    }
}

-(BookmarkEntry*)getNewBME
{
    return [NSEntityDescription insertNewObjectForEntityForName:@"BookmarkEntry" inManagedObjectContext:self.bmDocument.managedObjectContext];
}
	
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
