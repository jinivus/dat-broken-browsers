//
//  DetailViewController.m
//  BookmarkBrowser
//
//  Created by Matt on 8/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import "WebDetailViewController.h"
#import "BookmarkEntry.h"

@interface WebDetailViewController ()

@end

@implementation WebDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    if(![[[self URLBox] text] isEqual: @""] ) //If URL request isn't empty then make the webview request
    {
        [[self WebView] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[self URLBox] text]]]];
    }
    self.WebView.delegate = self; //Sets the delegate of the sub-webview as self
	// Do any additional setup after loading the view.
}
- (void)viewWillDisappear:(BOOL)animated
{   if(![[[self URLBox] text] isEqual: @""] )
    {
        if([self mode] == 2)
        {
            //[Bookmarks addEntry:[[self URLBox] text]];
            //[[self navigationController] popToRootViewControllerAnimated:YES];
        }
    }
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if([[request.URL scheme] isEqualToString:@"http"]){
        [self.URLBox setText:request.URL.standardizedURL.absoluteString];
    }
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)URLBoxAction:(id)sender {
    [[self WebView] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[self URLBox] text]]]];
    
}
- (IBAction)URLBoxEnd:(id)sender {
    [[self WebView] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[self URLBox] text]]]];
}
- (IBAction)URLBoxDidEndOnExit:(id)sender {
    if([self mode] == 2)
    {
            [[self navigationController] popToRootViewControllerAnimated:YES];
    }
    else
    {
        [[self WebView] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[self URLBox] text]]]];
    }
}
- (IBAction)AddBookmark:(id)sender { //Adds bookmark to the Bookmarks entries with defaults extracted from webpage
    //Bookmarks *bm = [Bookmarks getBookmarks];
    BookmarkEntry *bme = [[BookmarkEntry alloc]  init];
    bme.Name=[[self WebView] stringByEvaluatingJavaScriptFromString:@"document.title"]; //Gets title from webpage for "Name" BookmarkEntry
    bme.url=[[self URLBox] text];
    NSURL *tURL = [[NSURL alloc] initWithString:bme.url];
    NSURL *newURL = [tURL  URLByAppendingPathComponent:@"favicon.ico" isDirectory:NO]; //Gets favicon from webpage for Image in BookmarkEntry
    bme.imageurl=[newURL absoluteString];
    bme.Image = [NSData dataWithContentsOfURL:newURL];
    //[bm.Entries addObject:bme];
    //[Bookmarks Save];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Added Bookmark" message:[[self URLBox] text] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show]; //Popup to notify that it saved
}
@end
