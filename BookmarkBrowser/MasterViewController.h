//
//  MasterViewController.h
//  BookmarkBrowser
//
//  Created by Matt on 8/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "EditDetailViewController.h"

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate, EditDetailViewControllerDelegate>

@property UIManagedDocument *bmDocument;
@property NSFetchedResultsController *controller;

-(void) configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *) indexPath;

@end
