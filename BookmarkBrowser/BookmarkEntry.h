//
//  BookmarkEntry.h
//  BookmarkBrowser
//
//  Created by Matt on 22/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BookmarkEntry : NSManagedObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * imageurl;
@property (nonatomic, retain) id image;

@end
