//
//  EditDetailViewController.m
//  BookmarkBrowser
//
//  Created by Matt on 15/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import "EditDetailViewController.h"
#import "BookmarkEntry.h"

@interface EditDetailViewController ()

@end

@implementation EditDetailViewController

@synthesize NameOutlet;
@synthesize URLOutlet;
@synthesize ImageOutlet;
@synthesize ImageURLOutlet;
@synthesize cBME;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([self adding])
    {
        
    }
    else
    {

        [URLOutlet setText:cBME.url];
        [NameOutlet setText:cBME.name];
        [ImageURLOutlet setText:cBME.imageurl];
        if(cBME.image == nil)
        {
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:cBME.imageurl]];
            cBME.image = imageData;
        }
        ImageOutlet.image = [[UIImage alloc] initWithData:cBME.image];
    }
	// Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated
{
    cBME = [self.delegate getNewBME];
    cBME.name = [NameOutlet text];
    cBME.url = [URLOutlet text];
    cBME.imageurl = [ImageURLOutlet text];
    cBME.image = UIImagePNGRepresentation([ImageOutlet image]);
    if(cBME.image == nil)
    {
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:cBME.imageurl]];
        cBME.Image = imageData;
    }
    if([self adding])
    {
        [super viewWillDisappear:YES];
        //call delegate add shit
        [self.delegate addBookmarkEntry:cBME];
    }
    else
    {
        [super viewWillDisappear:YES];
        //[bm.Entries objectAtIndex:[self index]];
        //[bm.Entries replaceObjectAtIndex:[self index] withObject:bme];
        //[Bookmarks Save];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
