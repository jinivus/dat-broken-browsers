//
//  DetailViewController.h
//  BookmarkBrowser
//
//  Created by Matt on 8/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebDetailViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *URLBox;
@property (weak, nonatomic) IBOutlet UIWebView *WebView;
- (IBAction)AddBookmark:(id)sender;
- (IBAction)URLBoxDidEndOnExit:(id)sender;
- (IBAction)URLBoxEnd:(id)sender;
- (IBAction)URLBoxAction:(id)sender;

//@property (weak,nonatomic) id<UIWebViewDelegate> delegate;

@property int mode;


@end
