//
//  BookmarkEntry.m
//  BookmarkBrowser
//
//  Created by Matt on 22/04/13.
//  Copyright (c) 2013 Matt. All rights reserved.
//

#import "BookmarkEntry.h"


@implementation BookmarkEntry

@dynamic url;
@dynamic name;
@dynamic imageurl;
@dynamic image;

@end
